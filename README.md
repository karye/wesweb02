---
description: Introduktion till kursen webbutveckling 2
---

# Introduktion

## Centralt innehåll

Undervisningen i kursen ska behandla följande centrala innehåll:

* **Webbserverns och dynamiska webbplatsers funktionalitet.**
* **Utvecklingsprocessen för ett webbtekniskt projekt med målsättningar, krav, begränsningar, planering och uppföljning. Systemering, kodning, optimering, testning och driftsättning.**
* Dokumentation av utvecklingsprocess och färdig produkt inklusive kod och mjukvarugränssnitt.
* **Funktionen i ett eller flera programspråk för dynamiska webbplatser.**
* **Datalagring i relationsdatabas eller med annan teknik.**
* **Standardiserade datautbytesformat**, till exempel XML och **JSON**.
* **Kodning och dokumentation enligt vedertagen praxis för vald teknik.**
* **Applikationsarkitektur och separation av olika slags logik.**
* Kvalitetssäkring av dynamiska webbapplikationers funktionalitet, säkerhet och kodkvalitet.
* Säkerhet och sätt att identifiera hot och sårbarheter samt hur attacker kan motverkas genom effektiva åtgärder.

## **Konkretisering**

* [ ] Förstå hur en webbserver får en dynamisk webbsida att funka
* [ ] Kunna bygga en dynamisk webbplats med CRUD-funktionalitet
  * [ ] Infoga data
  * [ ] Hitta data
  * [ ] Ändra data
  * [ ] Radera data
* [ ] Kunna hämta JSON-data från webbtjänst på internet
* [ ] Kunna OOP
  * [ ] Skapa klasser

## Bra webblänkar

* [https://devdocs.io](https://devdocs.io)
* [http://orion.lnu.se/pub/education/course/1IK424/VT13/sessions/F01.html](http://orion.lnu.se/pub/education/course/1IK424/VT13/sessions/F01.html)
* [http://orion.lnu.se/pub/education/course/1IK424/VT13/sessions/F02.html](http://orion.lnu.se/pub/education/course/1IK424/VT13/sessions/F02.html#1)
* [http://orion.lnu.se/pub/education/course/1IK424/VT13/sessions/F03.html](http://orion.lnu.se/pub/education/course/1IK424/VT13/sessions/F03.html)

## Material
