# C för Skapa/Infoga

I detta avsnitt av kursen fokuserar vi på hur man skapar eller infogar nya poster i en databas, vilket är en av de grundläggande operationerna i CRUD (Create, Read, Update, Delete). Vi kommer att gå igenom processen att registrera en ny användare i en webbapplikation.

## Registrera användare

Att registrera en användare innebär att samla in data från ett formulär och spara denna information i en databas. Denna process kräver att vi skapar ett användargränssnitt (ett formulär) och en backend-logik för att hantera inskickade data.

### registrera.php

Denna fil innehåller både HTML och PHP-kod för att visa ett formulär där användare kan registrera sig samt PHP-kod för att hantera inskickad formulärdata och infoga den i en databas.

```php
<?php
// PHP version 7
// En enkel CRUD webbapp
include_once "./resurser/conn.php"; // Inkluderar databasanslutningsfilen

session_start();
if (!isset($_SESSION['anamn'])) { // Kontrollerar om användaren är inloggad
    header('Location: logga-in-db.php'); // Omdirigerar till inloggningssidan om inte inloggad
    exit;
}

// Läs av data från formuläret
$fnamn = filter_input(INPUT_POST, "fnamn", FILTER_SANITIZE_STRING);
$enamn = filter_input(INPUT_POST, "enamn", FILTER_SANITIZE_STRING);
$epost = filter_input(INPUT_POST, "epost", FILTER_SANITIZE_EMAIL);

// Kontrollera att data finns innan vi fortsätter
if ($fnamn && $enamn && $epost) {
    // Förbered SQL-frågan för att infoga data
    $sql = "INSERT INTO personer (fnamn, enamn, epost) VALUES (?, ?, ?);";
    $stmt = $conn->prepare($sql);
    $stmt->bind_param("sss", $fnamn, $enamn, $epost);
    $result = $stmt->execute();

    if ($result) {
        echo "<p>Personen är registrerad!</p>";
    } else {
        die("Det blev fel med SQL-satsen.");
    }

    $conn->close(); // Stänger ned anslutningen till databasen
}
?>
<!DOCTYPE html>
<html lang="sv">
<head>
    <!-- Här inkluderas meta-taggar, titel och länkar till CSS -->
</head>
<body>
    <div class="kontainer">
        <!-- Sidhuvud och navigationsmeny -->
        <main>
            <!-- Formulär för att registrera användare -->
        </main>
    </div>
</body>
</html>
```

I PHP-koden använder vi `filter_input` för att sanera och säkra data som tas emot från formuläret. Vi använder sedan en förberedd SQL-sats (`prepare` och `bind_param`) för att skydda mot SQL-injections, vilket är en viktig säkerhetsåtgärd.

### style.css

Denna CSS-fil innehåller stilar för att förbättra utseendet på registreringsformuläret och hela webbsidan. CSS-koden definierar typsnitt, bakgrund, layout och formatering av olika element på sidan.

```css
@import url('https://fonts.googleapis.com/css?family=Libre+Baskerville&display=swap');

body {
    background: #9db4b2 url("./bilder/samuel-ramos-1319769-unsplash.jpg");
}
.kontainer {
    background: #fff;
    width: 800px;
    margin: 1em auto;
    padding: 1em;
    border-radius: 10px;
    font-family: 'Libre Baskerville', serif;
}
/* Fler CSS-stilar för sidhuvud, formulär och tabeller */
```

I CSS-koden används `@import` för att inkludera ett externt typsnitt från Google Fonts. Bakgrund, layout och typsnittsstilar definieras för att skapa en visuellt tilltalande webbsida.

---

Genom att följa denna guide skapar du en grund för att hantera skapande av nya poster i din webbapplikation på ett säkert och effektivt sätt.