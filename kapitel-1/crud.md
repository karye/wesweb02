---
description: >-
  CRUD står för Create, Read, Update, Delete och är de grundläggande operationerna för att hantera data i en databas. Denna guide visar steg för steg hur du kan implementera dessa operationer i en webbapplikation med hjälp av PHP och MySQL.
---

# Vad är CRUD?

CRUD-operationer är grunden för de flesta webbapplikationer som hanterar data. De låter användare skapa, läsa, uppdatera och ta bort information i en databas. Detta är viktigt för att dynamiskt kunna hantera data på en webbsida.

## Databaskopplingen

För att din webbapplikation ska kunna kommunicera med en databas behöver du först upprätta en databaskoppling. Nedan följer en guide för hur du skapar en mapp för dina resurser, samt hur du skapar en PHP-fil för att koppla din applikation till en MySQL-databas.

### Katalogen resurser

För att organisera dina filer på ett bra sätt, börja med att skapa en mapp som heter **resurser**. Denna mapp kommer att innehålla alla filer som krävs för databaskopplingen.

#### conn.php

Inom mappen **resurser**, skapa en fil med namnet `conn.php`. Denna fil kommer att innehålla PHP-kod för att upprätta en koppling till din databas. Se till att ange rätt databasuppgifter som host, databasnamn, användarnamn och lösenord.

```php
<?php
// Aktivera felmeddelanden under utveckling för att lättare kunna felsöka
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$host = "localhost";
$databas = "register";
$anvandare = "register";
$losenord = "dittLösenord"; // Ändra till ditt faktiska lösenord

// Försök att logga in på mysql-servern och välj databas
$conn = new mysqli($host, $anvandare, $losenord, $databas);

// Kontrollera om anslutningen lyckades
if ($conn->connect_error) {
    die("Kunde inte ansluta till databasen: " . $conn->connect_error);
} else {
    // Anslutningen lyckades, kan använda $conn för att kommunicera med databasen
}
```

### Skydda mappen

Det är viktigt att skydda känslig information. Med en `.htaccess`-fil kan du ge en grundläggande skyddsnivå så att ingen utifrån kan komma åt mappens innehåll direkt.

#### .htaccess

Skapa en `.htaccess`-fil inom mappen **resurser** för att begränsa åtkomsten. Ange sökvägen till din `.htpasswd`-fil för att definiera vilka användare som har tillåtelse att åtkomma mappen.

```
AuthName        "Bara för medlemmar"
AuthType        Basic
AuthUserFile    /sökväg/till/din/.htpasswd
require         valid-user
```

## SQL-databasen

För att lagra och hantera data effektivt behöver du en välstrukturerad databas. Nedan följer steg för att skapa en databas och en tabell för att lagra användarinformation.

### Skapa en databas och en användare

Innan du kan skapa en tabell måste du ha en databas. Använd ditt databashanteringsverktyg (som phpMyAdmin) för att skapa en ny databas. Glöm inte att även skapa en användare med lämpliga rättigheter för att hantera databasen.

### Skapa en tabell

När databasen är på plats, skapa en tabell med namnet `personer` för att lagra information om användare. Nedan är SQL-koden för att skapa tabellen med de nödvändiga k

olumnerna för id, förnamn, efternamn och e-post.

```sql
CREATE TABLE `personer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fnamn` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `enamn` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `epost` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
```

Notera att `id`-kolumnen är inställd att autoinkrementeras. Detta innebär att varje ny post automatiskt får ett unikt id.

---

Genom att följa denna guide skapar du en solid grund för att hantera data i din webbapplikation med CRUD-operationer.