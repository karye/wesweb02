# U för Ändra/Redigera

Denna del av kursen fokuserar på hur du kan ändra eller redigera befintlig information i en databas, en central komponent i CRUD-operationerna. Vi kommer att gå igenom hur man skapar en form för att redigera en användares information baserat på den användare som valts från en lista.

## Lista alla användare

För att kunna redigera en användare behöver vi först lista alla användare och ge möjlighet att välja en användare att redigera. Detta har täckts i tidigare avsnitt. Efter att en användare har valts, navigeras användaren till en sida där de kan ändra användarens information.

### PHP- och HTML-struktur för Redigering

Nedan är koden för en webbsida där en användare kan redigera informationen för en specifik person baserad på personens ID. Notera att vi inkluderar konfigurationsfilen för databasanslutningen och använder sessioner för att hantera inloggning.

```php
<?php
// PHP version 7
// En enkel CRUD webbapp
include_once "{$_SERVER["DOCUMENT_ROOT"]}/_databaser/konfig-db.php";
session_start();
if (!isset($_SESSION['anamn'])) {
    header('Location: logga-in-db.php');
    exit;
}

if (isset($_GET["id"])) {
    $id = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_NUMBER_INT);

    // SQL-fråga för att hämta all data om användaren
    $sql = "SELECT * FROM personer WHERE id = ?";
    $stmt = $conn->prepare($sql);
    $stmt->bind_param("i", $id);
    $stmt->execute();
    $result = $stmt->get_result();

    if ($result->num_rows > 0) {
        $rad = $result->fetch_assoc();
    } else {
        die("Användaren med ID $id finns inte.");
    }
}
?>
<!DOCTYPE html>
<html lang="sv">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Adressregister</title>
    <link href="https://stackpath.bootstrapcdn.com/bootswatch/4.3.1/flatly/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <div class="kontainer">
        <header>
            <h1>Adressregister</h1>
            <nav>
                <ul class="nav nav-tabs">
                    <li class="nav-item"><a class="nav-link" href="logga-in-db.php">Logga in</a></li>
                    <li class="nav-item"><a class="nav-link" href="registrera-db.php">Registrera</a></li>
                    <li class="nav-item"><a class="nav-link active" href="lista-db.php">Lista</a></li>
                </ul>
            </nav>
        </header>
        <main>
            <form class="jumbotron" action="redigera-spara-db.php" method="post">
                <label>Förnamn</label>
                <input class="form-control" name="fnamn" type="text" value="<?php echo htmlspecialchars($rad['fnamn']); ?>" required>
                <label>Efternamn</label>
                <input class="form-control" name="enamn" type="text" value="<?php echo htmlspecialchars($rad['enamn']); ?>" required>
                <label>Epost</label>
                <input class="form-control" name="epost" type="email" value="<?php echo htmlspecialchars($rad['epost']); ?>" required>
                <input name="id" type="hidden" value="<?php echo $rad['id']; ?>">
                <button class="btn btn-warning">Uppdatera</button>
            </form>
        </main>
    </div>
</body>
</html>
```

I detta exempel har vi förbättrat säkerheten genom att använda `filter_input` för att sanera inkommande ID och `prepare`-metoden med `bind_param` för att skydda mot SQL-injections. Det är viktigt att validera och sanera användardata för att skapa en säker webbapplikation.

Efter att användaren har redigerat informationen och tryckt på "Uppdatera"-knappen, skickas datan till `redigera-spara-db.php` (som behöver implementeras) där ändringarna sparas i databasen. Detta ger en komplett och säker process för att redigera data i en webbapplikation.