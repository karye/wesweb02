# D för Radera

I denna del av kursen fokuserar vi på hur du kan radera eller ta bort information från en databas, vilket är en viktig del av CRUD-operationerna. Vi kommer att gå igenom processen att radera en användare från databasen baserat på användarens ID.

## Processen för att radera en användare

För att kunna radera en användare måste vi först ha ett sätt att identifiera vilken användare som ska tas bort. Detta görs typiskt genom att välja en användare från en lista, där varje användare är länkad till en raderingsfunktion med användarens unika ID.

### PHP- och HTML-struktur för Radering

Nedan är koden för en webbsida som hanterar radering av en användare baserad på det ID som skickas via en GET-förfrågan. Vi inkluderar databaskonfigurationsfilen, startar en session och kontrollerar om användaren är inloggad.

```php
<?php
// PHP version 7
// En enkel CRUD webbapp
include_once "{$_SERVER["DOCUMENT_ROOT"]}/_databaser/konfig-db.php";
session_start();
if (!isset($_SESSION['anamn'])) {
    header('Location: logga-in-db.php');
    exit;
}

if (isset($_GET['id'])) {
    $id = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_NUMBER_INT);

    // Förbered en SQL-sats för att radera användaren
    $sql = "DELETE FROM personer WHERE id = ?";
    $stmt = $conn->prepare($sql);
    $stmt->bind_param("i", $id);
    $result = $stmt->execute();

    // Kontrollera om raderingen lyckades
    if ($result) {
        echo "<p>Personen med id=$id har raderats!</p>";
    } else {
        die('Det blev fel med SQL-satsen.');
    }
} else {
    echo '<p>Något blev fel! Id saknas.</p>';
}
?>
<!DOCTYPE html>
<html lang="sv">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Adressregister</title>
    <link href="https://stackpath.bootstrapcdn.com/bootswatch/4.3.1/flatly/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <div class="kontainer">
        <header>
            <h1>Adressregister</h1>
            <nav>
                <ul class="nav nav-tabs">
                    <li class="nav-item"><a class="nav-link" href="logga-in-db.php">Logga in</a></li>
                    <li class="nav-item"><a class="nav-link" href="registrera-db.php">Registrera</a></li>
                    <li class="nav-item"><a class="nav-link" href="lista-db.php">Lista</a></li>
                </ul>
            </nav>
        </header>
        <main>
            <!-- Meddelande om raderingen visas här -->
        </main>
    </div>
</body>
</html>
```

I detta exempel har vi förbättrat säkerheten genom att använda `filter_input` för att sanera inkommande ID och `prepare`-metoden med `bind_param` för att skydda mot SQL-injections, vilket är avgörande för att skapa en säker webbapplikation. Genom att implementera en säker raderingsfunktion säkerställer vi att känslig information hanteras korrekt och att endast auktoriserade åtgärder kan genomföras.

Genom att följa denna guide får du en solid grund för att hantera radering av data i din webbapplikation på ett säkert och effektivt sätt.