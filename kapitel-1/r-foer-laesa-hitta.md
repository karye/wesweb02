# R för Läsa/Hitta

Denna del av kursen fokuserar på hur du kan läsa eller hitta information från en databas, en kritisk komponent i CRUD-operationerna. Vi kommer att gå igenom två centrala exempel: hur man loggar in användare och hur man listar alla användare registrerade i systemet.

## Logga in

Första exemplet visar hur en enkel inloggningsprocess kan implementeras med PHP. Vi använder sessions för att hålla reda på inloggade användare och en säker metod för att kontrollera lösenord.

### HTML-struktur och PHP-kod för inloggning

Nedan är koden för en enkel inloggningssida. Notera att vi inkluderar konfigurationsfilen för databasanslutningen, startar en session, och hanterar inloggningsförsök genom att kontrollera användarnamn och lösenord mot databasen.

```php
<?php
// PHP version 7
// En enkel CRUD webbapp
include_once "{$_SERVER["DOCUMENT_ROOT"]}/_databaser/konfig-db.php";
session_start();
?>
<!DOCTYPE html>
<html lang="sv">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Adressregister</title>
    <link href="https://stackpath.bootstrapcdn.com/bootswatch/4.3.1/flatly/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <div class="kontainer">
        <header>
            <h1>Adressregister</h1>
            <nav>
                <ul class="nav nav-tabs">
                    <li class="nav-item">
                        <?php if (!isset($_SESSION['anamn'])) { ?>
                            <a class="nav-link active" href="logga-in-db.php">Logga in</a>
                        <?php } else { ?>
                            <a class="nav-link active" href="logga-ut-db.php">Logga ut</a>
                        <?php } ?>
                    </li>
                </ul>
            </nav>
        </header>
        <main>
            <!-- Inloggningsformulär -->
            <form class="jumbotron" action="#" method="post">
                <label>Användarnamn</label>
                <input class="form-control" type="text" name="anamn" required>
                <label>Lösenord</label>
                <input class="form-control" type="password" name="losen" required>
                <button class="btn btn-primary">Logga in</button>
            </form>
        </main>
    </div>
</body>
</html>
```

I PHP-koden hanterar vi inloggningslogiken och använder `password_verify()` för att säkert jämföra det angivna lösenordet med hashvärdet lagrat i databasen. Om autentiseringen lyckas, omdirigeras användaren till en annan sida.

## Lista alla användare

Nästa exempel visar hur man kan lista alla användare som är registrerade i systemet. Detta är ett grundläggande sätt att läsa och visa data från en databas.

### HTML-struktur och PHP-kod för att lista användare

Koden nedan skapar en enkel webbsida som listar alla användare från databasen `personer`. Vi använder en tabell för att organisera och visa informationen på ett överskådligt sätt.

```php
<?php
// PHP version 7
// En enkel CRUD webbapp
include_once $_SERVER["DOCUMENT_ROOT"] . "/_databaser/konfig-db.php";
session_start();
if (!isset($_SESSION['anamn'])) {
    header('Location: logga-in-db.php');
    exit;
}
?>
<!DOCTYPE html>
<html lang="sv">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Adressregister</title>
    <link href="https://stackpath.bootstrapcdn.com/bootswatch/4.3.1/flatly/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <div class="kont

ainer">
        <header>
            <h1>Adressregister</h1>
            <nav>
                <ul class="nav nav-tabs">
                    <li class="nav-item">
                        <a class="nav-link" href="logga-in-db.php">Logga in</a>
                        <a class="nav-link" href="logga-ut-db.php">Logga ut</a>
                        <a class="nav-link" href="registrera-db.php">Registrera</a>
                        <a class="nav-link active" href="lista-db.php">Lista</a>
                    </li>
                </ul>
            </nav>
        </header>
        <main>
            <!-- Tabell för att visa användare -->
            <table class="table table-striped">
                <tr>
                    <th>Förnamn</th>
                    <th>Efternamn</th>
                    <th>Epost</th>
                    <th colspan="2">Åtgärder</th>
                </tr>
                <?php
                // SQL-fråga för att hämta alla användare
                $sql = "SELECT * FROM personer";
                $result = $conn->query($sql);
                if ($result) {
                    while ($rad = $result->fetch_assoc()) {
                        echo "<tr>";
                        echo "<td>{$rad['fnamn']}</td>";
                        echo "<td>{$rad['enamn']}</td>";
                        echo "<td>{$rad['epost']}</td>";
                        echo "<td><a class=\"btn btn-outline-danger\" href=\"radera-verifiera-db.php?id={$rad['id']}\">Radera</a></td>";
                        echo "<td><a class=\"btn btn-outline-warning\" href=\"redigera-db.php?id={$rad['id']}\">Redigera</a></td>";
                        echo "</tr>";
                    }
                }
                $conn->close();
                ?>
            </table>
        </main>
    </div>
</body>
</html>
```

I detta exempel visar vi hur man kan skapa en responsiv och användarvänlig webbsida för att visa en lista över alla registrerade användare. Genom att inkludera länkar för att redigera och radera användare ger vi också en grundläggande hantering av användardata.

Dessa exempel ger en bra startpunkt för att bygga en webbapplikation som använder CRUD-operationer för att hantera användardata.