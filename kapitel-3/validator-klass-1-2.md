# Validator klass 1:2

## Introduktion till Validering av Formulärdata

Validering av formulärdata är en viktig del av webbutveckling för att säkerställa att användarinput är korrekt och säker innan den behandlas eller lagras. I denna del kommer vi att skapa en `Validator`-klass som kan användas för att validera användarnamn och e-postadresser som matas in via ett formulär.

## Skapa ett formulär med Bootstrap

För att demonstrera validering, börjar vi med att skapa ett enkelt formulär som använder Bootstrap för att skapa ett snyggt och responsivt användargränssnitt.

```html
<!DOCTYPE html>
<html lang="sv">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="style.css">
    <title>Formulärvalidering</title>
</head>
<body>
    <div class="kontainer">
        <h1>Ett formulär</h1>
        <form action="#" method="post">
            <div class="mb-3">
                <label for="username" class="form-label">Username</label>
                <input type="text" class="form-control" id="username" name="username" placeholder="Användarnamn" required>
            </div>
            <div class="mb-3">
                <label for="email" class="form-label">Email address</label>
                <input type="email" class="form-control" id="email" name="email" placeholder="name@example.com" required>
            </div>
            <button name="formulär" type="submit" class="btn btn-primary">Skicka</button>
        </form>
    </div>
</body>
</html>
```

## Skapa `Validator`-klassen

`Validator`-klassen kommer att innehålla logik för att validera olika typer av användarinput. Vi börjar med att skapa klassens grundstruktur.

```php
<?php
class Validator {
    private $data;
    private $errors = [];

    public function __construct($postData) {
        $this->data = $postData;
    }

    public function validateUsername() {
        // Valideringslogik för användarnamn
    }

    public function validateEmail() {
        // Valideringslogik för e-postadress
    }
}
```

## Använda `Validator`-klassen

När en användare skickar formuläret, använder vi `Validator`-klassen för att validera datan.

```php
<?php
include "Validator.php";

if (isset($_POST['formulär'])) {
    $validator = new Validator($_POST);
    $validator->validateUsername();
    $validator->validateEmail();
    // Ytterligare valideringsmetoder kan kallas här
}
?>
```

## Implementera Valideringsmetoder

Här är exempel på hur man kan implementera valideringsmetoderna i `Validator`-klassen.

```php
public function validateUsername() {
    if (strlen($this->data['username']) < 5 || strlen($this->data['username']) > 15) {
        $this->errors['username'] = "Användarnamnet måste vara mellan 5 till 15 tecken.";
    }
}

public function validateEmail() {
    if (!filter_var($this->data['email'], FILTER_VALIDATE_EMAIL)) {
        $this->errors['email'] = "Ogiltigt e-postformat.";
    }
}
```

## Visa Felmeddelanden

För att visa eventuella felmeddelanden till användaren kan du lägga till en metod i `Validator`-klassen som samlar ihop och returnerar felmeddelandena.

```php
public function getErrors() {
    return $this->errors;
}
```

Och sedan använda den

 i din HTML för att visa felmeddelanden:

```php
<?php if (!empty($validator->getErrors())): ?>
    <div class="alert alert-danger">
        <?php foreach ($validator->getErrors() as $error): ?>
            <p><?php echo $error; ?></p>
        <?php endforeach; ?>
    </div>
<?php endif; ?>
```

Genom att följa dessa steg har du skapat en `Validator`-klass som kan validera användarinput från ett formulär, och du har implementerat logik för att visa felmeddelanden till användaren. Detta tillvägagångssätt gör det enkelt att återanvända valideringslogiken i olika delar av din applikation.