# Intro till API

I denna del av kursen kommer vi att utforska grunderna i att använda API:er (Application Programming Interfaces) för att hämta data från tjänster på internet. Vi kommer att titta på vad ett API är, hur JSON-formatet används för att överföra data, och vi kommer att prova att hämta data från ett publikt API.

## Vad är ett API?

Ett API (Application Programming Interface) är en uppsättning regler och specifikationer som mjukvaruapplikationer kan följa för att kommunicera med varandra. API:er används ofta för att tillåta externa program att hämta eller skicka data till en tjänst på ett standardiserat sätt.

### JSON-formatet

JSON (JavaScript Object Notation) är ett lättläst datautbytesformat som används för att överföra data mellan klient och server. Det är baserat på text och använder nyckel-värdepar för att representera data, vilket gör det idealiskt för användning i webbapplikationer och API-kommunikation.

För att göra JSON-data lättare att läsa i webbläsaren kan du installera ett tillägg som JSON Formatter. Detta tillägg formaterar JSON på ett överskådligt sätt direkt i webbläsaren.

## Ett publikt API

Ett exempel på ett publikt API är [api.chucknorris.io](https://api.chucknorris.io), som erbjuder ett urval av skämt om Chuck Norris. Vi kommer att använda detta API för att hämta ett slumpmässigt skämt i JSON-format.

### Koden för att hämta data från API

För att hämta data från ett API i PHP kan vi använda funktionen `file_get_contents()` för att göra en GET-förfrågan till API:et. Svaret kommer vanligtvis i JSON-format, vilket vi sedan kan avkoda med `json_decode()` för att enkelt komma åt datan i PHP.

#### chucknorris.php

```php
<?php
// Tjänsten
$url = "https://api.chucknorris.io/jokes/random";

// Anropa API:et
$json = file_get_contents($url);

// Avkoda JSON
$jsonData = json_decode($json);

// Plocka ut skämtet
$skamtet = $jsonData->value;

// Plocka ut bilden
$bilden = $jsonData->icon_url;

// Skriv ut skämtet och bilden
echo "<blockquote><img src=\"$bilden\" alt=\"Chuck Norris\">
$skamtet<footer>Chuck Norris</footer></blockquote>";
?>
```

#### style.css

CSS-koden nedan används för att ge en grundläggande styling till vår webbsida, inklusive bakgrund, layout och typsnitt.

```css
/* Här inkluderas den fullständiga CSS-koden som tidigare specificerats */
```

### JSON-formatet

JSON-data består av nyckel-värdepar. I vårt exempel med Chuck Norris-skämt innehåller JSON-objektet nycklar som `id`, `value` och `icon_url`, där `value` representerar själva skämtet och `icon_url` URL:en till en bild av Chuck Norris.

### Sammantaget

Genom att kombinera PHP-koden och CSS-styling får vi en komplett webbsida som hämtar och visar ett slumpmässigt Chuck Norris-skämt från det publika API:et.

### Tutorial

För en mer omfattande guide om hur man arbetar med JSON i PHP, rekommenderas [PHP JSON complete tutorial](https://alexwebdevelop.com/php-json-backend/), som ger en djupare förståelse för hur man hanterar JSON-data i backend-applikationer.

Genom att följa denna guide får du en god förståelse för grunderna i API-användning, JSON-formatet, och hur man kan hämta och visa data från externa tjänster i dina egna webbapplikationer.