# Labb 5 - OMDB api

![](<../.gitbook/assets/image (11).png>)

## Formuläret

Vi bygger ett enkelt formulär mha [Bootstrap](https://getbootstrap.com):

```php
<!DOCTYPE html>
<html lang="sv">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>OMDB</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
</head>

<body>
    <div class="kontainer">
        <h1>Lista filmer på OMDB</h1>
        <!-- Formulär för att mata in sökord = del-av-film -->

        <main>
            <?php
            // Ta emot data från formuläret
            $sokterm = filter_input(INPUT_POST, "sokterm");

            // Om data finns
            if ($sokterm) {

                // Url:en till OMDB api
                $url = "https://www.omdbapi.com/?apikey=ea805224";

                // Hämta data, avkoda, lista alla filmer med bild och texter
                // Steg 1. Hämta data

                // Steg 2. Avkoda JSON

                // Steg 3. Plocka det som intresserar oss

            }
            ?>
        </main>
    </div>
</body>

</html>
```

## Exempel på anrop

Om vi söker på Bond-filmer skriver vi:

```
https://www.omdbapi.com/?apikey=ea805224&s=bond
```

### JSON-svaret

JSON-svaret vi får från api:et ser ut såhär:

```json
{
    "Search": [
        {
            "Title": "Becoming Bond",
            "Year": "2017",
            "imdbID": "tt6110504",
            "Type": "movie",
            "Poster": "https://m.media-amazon.com/images/M/MV5BZDRiNGIzYWEtYzY3Zi00MTcwLWFkOWItZjEyYjg0OGMxMGQ5XkEyXkFqcGdeQXVyMjM4NjQxMDA@._V1_SX300.jpg"
        },
        {},
        {},
        {},
        {},
        {},
        {},
        {},
        {},
        {}
    ],
    "totalResults": "465",
    "Response": "True"
}
```

## Förstå JSON-svaret

### 1:a nivån

Den första nivån har 3 nycklar:

```json
{
    "Search": [],
    "totalResults": "465",
    "Response": "True"
}
```

#### PHP-kod

För att plocka tex **Search**:

```php
$Search = $dataJson->Search;  
```

### 2: nivån&#x20;

**Search**-objektet är en array:

```json
"Search": [
    {},
    {},s
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {}
]
```

#### PHP-kod

**Search**-objektet är en array måste vi loopa igenom den:

```php
// Loopa igenom arrayen
foreach ($Search as $film) {
    ...
}
```

### 3:e nivån

Varje objekt i arrayen består av 5 nycklar:

```json
// Some code
Search": [
    {
        "Title": "Becoming Bond",
        "Year": "2017",
        "imdbID": "tt6110504",
        "Type": "movie",
        "Poster": "https://m.media-amazon.com/images/M/MV5BZDRiNGIzYWEtYzY3Zi00MTcwLWFkOWItZjEyYjg0OGMxMGQ5XkEyXkFqcGdeQXVyMjM4NjQxMDA@._V1_SX300.jpg"
    },
    ..
]
```

#### PHP-kod

För att nu plocka tex **Title** gör vi:

```php
// Loopa igenom arrayen
foreach ($Search as $film) {
    $Title = $film->Title;
    ...
}
```

## Styla resultatet

Vi använder [Bootstrap Cards](https://getbootstrap.com/docs/5.1/components/card/) för ett snyggt resultat:

![](<../.gitbook/assets/image (13).png>)
