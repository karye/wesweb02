---
description: Söka artist på iTunes musikdatabas
---

# Labb 2 - itunes

## Resultat

![](<../.gitbook/assets/image (11) (1) (1) (1).png>)

## Api:et

* [https://affiliate.itunes.apple.com/resources/documentation/itunes-store-web-service-search-api](https://affiliate.itunes.apple.com/resources/documentation/itunes-store-web-service-search-api)

### JSON-svaret

```json
{
  "wrapperType": "track",
  "kind": "song",
  "artistId": 909253,
  "collectionId": 120954021,
  "trackId": 120954025,
  "artistName": "Jack Johnson",
  "collectionName": "Sing-a-Longs and Lullabies for the Film Curious George",
  "trackName": "Upside Down",
  "collectionCensoredName": "Sing-a-Longs and Lullabies for the Film Curious George",
  "trackCensoredName": "Upside Down",
  "artistViewUrl": "https://itunes.apple.com/WebObjects/MZStore.woa/wa/viewArtist?id=909253",
  "collectionViewUrl": "https://itunes.apple.com/WebObjects/MZStore.woa/wa/viewAlbum?i=120954025&id=120954021&s=143441",
  "trackViewUrl": "https://itunes.apple.com/WebObjects/MZStore.woa/wa/viewAlbum?i=120954025&id=120954021&s=143441",
  "previewUrl": "http://a1099.itunes.apple.com/r10/Music/f9/54/43/mzi.gqvqlvcq.aac.p.m4p",
  "artworkUrl60": "http://a1.itunes.apple.com/r10/Music/3b/6a/33/mzi.qzdqwsel.60x60-50.jpg",
  "artworkUrl100": "http://a1.itunes.apple.com/r10/Music/3b/6a/33/mzi.qzdqwsel.100x100-75.jpg",
  "collectionPrice": 10.99,
  "trackPrice": 0.99,
  "collectionExplicitness": "notExplicit",
  "trackExplicitness": "notExplicit",
  "discCount": 1,
  "discNumber": 1,
  "trackCount": 14,
  "trackNumber": 1,
  "trackTimeMillis": 210.743,
  "country": "USA",
  "currency": "USD",
  "primaryGenreName": "Rock"
}
```

## Sökformuläret

{% tabs %}
{% tab title="itunes.php" %}
```php
<!DOCTYPE html>
<html lang="sv">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Väderdata</title>
    <link rel="stylesheet" href="./style.css">
</head>
<body>
    <div class="kontainer">
        <h1>ITunes musikdatabas sök</h1>
        <form action="#" method="post">
            ...
        </form>
    </div>
</body>
</html>
```
{% endtab %}

{% tab title="style.css" %}
```css
/* Enkel CSS-reset */
html {
    box-sizing: border-box;
}
*, *:before, *:after {
    box-sizing: inherit;
}
body, h1, h2, h3, h4, h5, h6, p, ul {
    margin: 0;
    padding: 0;
}
body {
    background: #F9F6EB url("./bilder/photo-1577271015616-9b74c52daace.jpg") fixed;
    background-size: cover;
}
.kontainer {
    width: 700px;
    padding: 2em;
    margin: 3em auto;
    background: #fff;
    border-radius: 5px;
    font-family: 'Source Sans Pro', sans-serif;
    border: 2px solid #a0a0a0;
    color: #4e4e4e;
}
#map {
    position: absolute;
    top: 0;
    bottom: 0;
    width: 100%;
}
form {
    text-align: center;
    margin: 40px;
}
table {
    width: 500px;
    border-collapse: collapse;
    margin: auto;
}
th {
    font-size: 30px;
}
td {
    padding: 5px;
}
tr:nth-child(even) {
    background: #CCC
}
tr:nth-child(odd) {
    background: #FFF
}
.kol2 {
    margin: 1em 0;
    display: grid;
    grid-template-columns: 1fr 2fr;
    grid-gap: 1em;
}
.kol3 {
    margin: 1em 0;
    display: grid;
    grid-template-columns: 1fr 2fr 1fr;
    grid-gap: 1em;
}
label {
    text-align: right;
    align-self: center;
    font-size: 0.9em;
}
input, textarea {
    padding: 0.7em;
    border-radius: 0.3em;
    border: 1px solid #ccc;
    font-weight: bold;
    box-shadow: inset 0 2px 2px rgba(0, 0, 0, 0.1);
}
button {
    margin: 1em 0;
    padding: 0.7em;
    border-radius: 0.3em;
    border: none;
    font-weight: bold;
    color: #FFF;
    background-color: #55a5d2;
    cursor: pointer;
}
footer {
    background: #9b9b9b;
    margin-top: 1em;
    padding: 1em;
    border-radius: 5px;
}
footer img {
    width: 50px;
}
```
{% endtab %}
{% endtabs %}

## PHP backend

```php
<?php
    ... = filter_input(INPUT_POST, "...");
    if (...) {
        // cst = correct search term
        ...
        $json = file_get_contents(...);

        $jsonData = json_decode($json);
        $resultCount = $jsonData->resultCount;
        ...

        echo "<table><tr><th>Top 5 songs by $resultCount</th></tr>";
        ...
        echo "</table>";
    }
?>
```
