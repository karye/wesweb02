---
description: Installera alla verktyg, konfigurera alla inställningar, skapar alla mappar
---

# Utvecklingsmiljö

## Installera webbeditorn VS Code & tillägg

* Installera [VS Code](https://code.visualstudio.com)
* Installera [git-scm](https://git-scm.com)
* Installera tilläggen:
  * [Beautify](https://marketplace.visualstudio.com/items?itemName=HookyQR.beautify)
  * [Path Intellisense](https://marketplace.visualstudio.com/items?itemName=christian-kohler.path-intellisense)
  * [PHP Intelephense](https://marketplace.visualstudio.com/items?itemName=bmewburn.vscode-intelephense-client)
  * [Docker](https://marketplace.visualstudio.com/items?itemName=ms-azuretools.vscode-docker)
  * [VSCode Great Icons](https://marketplace.visualstudio.com/items?itemName=emmanuelbeziat.vscode-great-icons)

## Installera PHP och konfigurera VS Code

1. Ladda ned [PHP 7.4 x64 Thread Safe 2](https://windows.php.net/download)
2. Packa upp till c:/php
3. Öppna VS Code
4. Fyll i rätt sökväg se [https://code.visualstudio.com/docs/languages/php](https://code.visualstudio.com/docs/languages/php)

## Skapa en utvecklingsmiljö

* Skapa ett konto på [github.com](https://github.com)
* Skapa en mapp **c:/github/webbsrvpgm2**

![](<.gitbook/assets/image (9).png>)

* Publicera mappen på [github.com](https://github.com)

![](<.gitbook/assets/image (10).png>)

## Installera Docker Desktop och LAMP-server

### LAMP = Linux Apache Mysql PHP

* Installera [Docker Desktop](https://docs.docker.com/docker-for-windows/install/)
* Gå in i BIOS och aktivera virtualisering
* Öppna terminalen i VS Code och kör kommandot
* Om du använder powershell:

```bash
docker run -d --restart unless-stopped -p 8080:80 -p 10000:10000 -v "c:\github\...:/var/www" -v mysql-data:/var/lib/mysql --name lamp karye/lampw
```

* Om du använder bash:&#x20;

```bash
docker run -d --restart unless-stopped -p 8080:80 -p 10000:10000 -v /host_mnt/c/github/...:/var/www -v mysql-data:/var/lib/mysql --name lamp karye/lampw
```

* Instruktioner till hur du använder LAMPW finns på [https://hub.docker.com/r/karye/lampw](https://hub.docker.com/r/karye/lampw)

## **PHP snippets**

* Skapa User Snippets för PHP

```php
{
    "PHP block": {
        "scope": "html,php",
        "prefix": "php",
        "body": [
            "<?php",
            "$1",
            "?>"
        ],
        "description": "PHP block"
    },
    "PHP p": {
        "scope": "html,php",
        "prefix": "echop",
        "body": [
            "echo \"<p>$1</p>\";"
        ],
        "description": "PHP p-tag"
    },
    "PHP sidhuvud": {
        "scope": "html,php",
        "prefix": "sidhuvud",
        "body": [
            "<?php",
            "/**",
            "* PHP version 7",
            "* @category   $1",
            "* @author     ... ... <...@gmail.com>",
            "* @license    PHP CC",
            "*/",
            "?>"
        ],
        "description": "PHP kommentarsblock som beskriver koden på sidan"
    },
    "PHP for-loop": {
        "scope": "html,php",
        "prefix": "forloop",
        "body": [
            "for (\\$i = 0; \\$i < $1; \\$i++) {",
                "\t$2",
            "}"
        ],
        "description": "PHP for-loop"
    }
```
